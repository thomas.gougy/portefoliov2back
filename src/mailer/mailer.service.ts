import { Injectable } from '@nestjs/common';
import * as nodemailer from "nodemailer"

@Injectable()
export class MailerService {

    private async transporter() {
        const testAccount = await nodemailer.createTestAccount()
        const transport = nodemailer.createTransport({
            host: "localhost",
            port: 1025,
            ignoreTLS: true,
            auth: {
                user: testAccount.user,
                pass: testAccount.pass
            }
        })

        return transport
    }

    async sendSignUpConfirmation(userEmail: string) {
        (await this.transporter()).sendMail({
            from: "noreply@tournamentapp.com",
            to: userEmail,
            subject: "inscription",
            html: "<h1>Yo les mans</h1>"
        })
    }

    async sendResetPasswordConfirmation(userEmail: string,url: string, temporaryPassword : string){
        (await this.transporter()).sendMail({
            from: "noreply@tournamentapp.com",
            to: userEmail,
            subject: "Reset your password",
            html: `<h3>voici votre password temporaire</h3>
            <h1> ${temporaryPassword} </h1> 
            <a href="${url}">reset your password</a>
            <p>Code will expired in 20 minutes</p>`
            

        })
    }
}
