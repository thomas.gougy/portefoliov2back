import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateFormationDto, UpdateFormationDto } from './dto/formation.dto';
import { NotFoundError } from 'rxjs';
import { v4 } from 'uuid';

@Injectable()
export class FormationService {

    constructor(private prismaService : PrismaService){

    }

    async getUserFormation(userId: string){

        const user = await this.prismaService.user.findUnique({where: {userId}})
        if(!user) throw new NotFoundException("user can't be found")
        return this.prismaService.formation.findMany({where: {userId}})
    }

    async getOneFormation(formationId: string){
        const formation = await this.verifyIfFormationExist(formationId)
        return formation
    }

    async createOneFormation(formationDto: CreateFormationDto){

       const formation = await this.prismaService.formation.create({data: formationDto})
       return formation
    }

    async createMultipleFormation(formationDto: CreateFormationDto[]){
        let defineFormation: any[] = []
        let formationIds : string [] = []
        let id

        for(let formation of formationDto){
            id = v4()
            defineFormation.push({formationId: id, ...formation})
            formationIds.push(id)
        }
        await this.prismaService.formation.createMany({data: defineFormation})

        return this.prismaService.formation.findMany({where:{
            formationId: {in: formationIds}
        }})
    }

    async updateOneFormation(formationDto: UpdateFormationDto){

        const formation = await this.verifyIfFormationExist(formationDto.formationId)

        const updatedFormation: UpdateFormationDto = {
            formationId: formation.formationId,
            userId: formation.userId,
            ...formationDto
        }

        return this.prismaService.formation.update({where:{formationId: formation.formationId}, data: updatedFormation})


    }

    async deleteOneFormation(formationId: string){

        const formation = await this.verifyIfFormationExist(formationId)

        await this.prismaService.formation.delete({where: {formationId: formationId}})

        return {msg: "formation has been deleted"}
    }

    async deleteMultipleFormation (formationIds: string[]){

        for(let id of formationIds){
            await this.deleteOneFormation(id)
        }
        
        return {msg: "Formations has been deleted"}
    }



    async verifyIfFormationExist(formationId: string){

        const formation = await this.prismaService.formation.findUnique({where:{formationId: formationId}})

        if(!formation) throw new NotFoundException("Formation can't be found")

        return formation
    }


}
