

/*formationId: string 
    formationName: string 
    level: string 
    canadaEquivalent: string 
    recognized :boolean
    startDate : Date
    endDate: Date
    userId : string 
*/

import { IsBoolean, IsDate, IsNotEmpty, IsString } from "class-validator"


export class CreateFormationDto{
    @IsString() 
    @IsNotEmpty()
    formationName: string 
    @IsString() 
    @IsNotEmpty()
    level: string
    @IsString() 
    @IsNotEmpty() 
    canadaEquivalent: string 
    @IsBoolean()
    recognized :boolean
    @IsString()
    startDate : Date
    @IsString()
    endDate: Date
    @IsString() 
    @IsNotEmpty()
    userId : string 
}

export class UpdateFormationDto{
    @IsString() 
    @IsNotEmpty()
    formationId: string
    @IsString() 
    @IsNotEmpty()
    formationName: string 
    @IsString() 
    @IsNotEmpty()
    level: string
    @IsString() 
    @IsNotEmpty() 
    canadaEquivalent: string 
    @IsBoolean()
    recognized :boolean
    @IsString()
    startDate : Date
    @IsString()
    endDate: Date
    @IsString() 
    @IsNotEmpty()
    userId : string 
}