import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { FormationService } from './formation.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateFormationDto, UpdateFormationDto } from './dto/formation.dto';


@ApiTags('Formation')
@Controller('formation')
export class FormationController {

    constructor(private formationService: FormationService){}

    @Get("user/:id")
    getUserFormation(@Param('id')userId: string){
       return this.formationService.getUserFormation(userId)
    }

    @Get(":id")
    getOneFormation(@Param('id')formationId: string){
        return this.formationService.getOneFormation(formationId)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("")
    createOneFormation(@Body()formationDto: CreateFormationDto){
        return this.formationService.createOneFormation(formationDto)
    }


    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("multiple")
    createMultipleFormation(@Body()formationDto: CreateFormationDto[]){
        return this.formationService.createMultipleFormation(formationDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Put()
    updateOneFormation(@Body()formationDto: UpdateFormationDto){
        return this.formationService.updateOneFormation(formationDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Delete("/:id")
    deleteOneFormation(@Param("id")formationId: string){
        return this.formationService.deleteOneFormation(formationId)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("delete/multiple")
    deleteMultipleFormation(@Body()formationIds: string[]){
        return this.formationService.deleteMultipleFormation(formationIds)
    }



}
