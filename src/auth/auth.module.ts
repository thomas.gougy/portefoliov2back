import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { jwtConstant } from './constant';
import { JwtStrategy } from './strategy.service';

@Module({
  imports: [JwtModule.register({
    global: true,
    secret: jwtConstant.secret,
    signOptions: {expiresIn: "30 days"}
  })],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy]
})
export class AuthModule {}
