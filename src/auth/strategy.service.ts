import { Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { PrismaService } from "src/prisma/prisma.service";

type Payload = {
    sub: string,
    email: string,
    firstname: string,
    lastname: string
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(private configService: ConfigService, private prismaService: PrismaService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get("JWT_SECRET"),
            ignoreExpiration: true
        })
    }


    async validate(payload: Payload) {

        const user = await this.prismaService.user.findUnique({ where: { email: payload.email } })
        if (!user) throw new UnauthorizedException("Unauthorized")
        Reflect.deleteProperty(user, "password")
        return user
    }

}