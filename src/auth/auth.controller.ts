import { Body, Controller, Delete, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ResetPasswordConfirmationDto, ResetPasswordDto, SignInDto, SignUpDto } from './dto/auth.dto';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';


@ApiTags('Auth')
@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService) { }

    @Post("signup")
    signup(@Body() signupDto: SignUpDto) {
        return this.authService.signup(signupDto)
    }
    @Post("signin")
    signin(@Body() signinDto: SignInDto) {
        return this.authService.signin(signinDto)
    }

    @Post("reset-password")
    restePassword(@Body() resetPasswordDto: ResetPasswordDto) {
        return this.authService.resetPassword(resetPasswordDto)
    }

    @Post("reset-password-confirmation")
    restePasswordConfirmation(@Body() resetPasswordConfirmationDto: ResetPasswordConfirmationDto) {
        return this.authService.resetPasswordConfirmation(resetPasswordConfirmationDto)
    }

    /*

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Delete("delete")
    deleteAccount(@Req() request: Request) {
        const user = request.user["userId"]


        return this.authService.deleteAccount(user)
    }
    */
}
