import { env } from "process";

export const jwtConstant = {
    secret: process.env.JWT_SECRET
}