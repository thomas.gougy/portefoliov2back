import { Module } from '@nestjs/common';

import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { ConfigModule } from '@nestjs/config';
import { MailerModule } from './mailer/mailer.module';
import { ExperienceModule } from './experience/experience.module';
import { FormationModule } from './formation/formation.module';
import { CompetenceModule } from './competence/competence.module';

@Module({
  imports: [ ConfigModule.forRoot({isGlobal: true}), AuthModule, PrismaModule, MailerModule, ExperienceModule, FormationModule, CompetenceModule],
})
export class AppModule {}
