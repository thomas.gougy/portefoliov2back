import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { CompetenceService } from './competence.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { CreateCompetenceDto, UpdateCompetenceDto } from './dto/competence.dto';

@ApiTags("Competences")
@Controller('competence')
export class CompetenceController {

    constructor(private competenceService: CompetenceService){}

    @Get("user/:id")
    getUserCompetence(@Param('id')userId: string){
       return this.competenceService.getUserCompetence(userId)
    }

    @Get(":id")
    getOneCompetence(@Param('id')competenceId: string){
        return this.competenceService.getOneCompetence(competenceId)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("")
    createOneCompetence(@Body()competenceDto: CreateCompetenceDto){
        return this.competenceService.createOneCompetence(competenceDto)
    }


    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("multiple")
    createMultipleFormation(@Body()competenceDto: CreateCompetenceDto[]){
        return this.competenceService.createMultipleCompetence(competenceDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Put()
    updateOneFormation(@Body()competenceDto: UpdateCompetenceDto){
        return this.competenceService.updateOneCompetence(competenceDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Delete("/:id")
    deleteOneFormation(@Param("id")competenceId: string){
        return this.competenceService.deleteOneCompetence(competenceId)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("delete/multiple")
    deleteMultipleFormation(@Body()competencesIds: string[]){
        return this.competenceService.deleteMultipleCompetence(competencesIds)
    }


}
