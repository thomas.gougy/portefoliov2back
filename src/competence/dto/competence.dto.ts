
/*
    competenceId : string 
    name : string
    level : number
    type : string
    userId :string 
*/

import { IsNotEmpty, IsNumber, IsString } from "class-validator"

export class CreateCompetenceDto{
    @IsString()
    @IsNotEmpty()
    name : string
    @IsNumber()
    @IsNotEmpty()
    level : number
    @IsString()
    @IsNotEmpty()
    type : string
    @IsString()
    @IsNotEmpty()
    userId :string 
}


export class UpdateCompetenceDto{
    @IsString()
    @IsNotEmpty()
    competenceId : string 
    @IsString()
    @IsNotEmpty()
    name : string
    @IsNumber()
    @IsNotEmpty()
    level : number
    @IsString()
    @IsNotEmpty()
    type : string
    @IsString()
    @IsNotEmpty()
    userId :string 
}

