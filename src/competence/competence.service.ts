import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateCompetenceDto, UpdateCompetenceDto } from './dto/competence.dto';
import { v4 } from 'uuid';

@Injectable()
export class CompetenceService {

    constructor(private prismaService: PrismaService){}

    async getUserCompetence(userId: string){

        const user = await this.prismaService.user.findUnique({where: {userId}})

        if(!user) throw new NotFoundException("user can't be found")

        return this.prismaService.competence.findMany({where: {userId}})

    }

    async getOneCompetence(competenceId: string){
        const competence  = await this.verifyCompetenceExist(competenceId)
        return competence
    }

    async createOneCompetence(competenceDto: CreateCompetenceDto){

        return this.prismaService.competence.create({data: competenceDto})

    }

    async createMultipleCompetence(competenceDto: CreateCompetenceDto[]){
        let id: string 
        let competenceIds: string [] = []
        let definedCompetence = []

        for(let competence of competenceDto){
            id = v4()
            competenceIds.push(id)
            definedCompetence.push({competenceId: id, ...competence})
        }

        await this.prismaService.competence.createMany({data: definedCompetence})

        return this.prismaService.competence.findMany({where: {competenceId: {in: competenceIds}}})
    }

    async updateOneCompetence(competenceDto: UpdateCompetenceDto){

        const competence = await this.verifyCompetenceExist(competenceDto.competenceId)

        const updatedCompetence: UpdateCompetenceDto = {...competenceDto ,competenceId : competence.competenceId, userId: competence.userId }

        return this.prismaService.competence.update({where:{competenceId: updatedCompetence.competenceId}, data: updatedCompetence})

    }

    async deleteOneCompetence(competenceId: string){

        const competence = await this.verifyCompetenceExist(competenceId)

        await this.prismaService.competence.delete({where: {competenceId}})

        return {msg: "competence has been deteled"}
    }

    async deleteMultipleCompetence(competenceId: string []){

        for(let id of competenceId){
            await this.deleteOneCompetence(id)
        }

        return {msg: "competences has been deleted"}
    }



    async verifyCompetenceExist(competenceId: string){

        const competence = await this.prismaService.competence.findUnique({where: {competenceId: competenceId}})

        if(!competence) throw new NotFoundException("Experience can't be found")

        return competence
    }
}
