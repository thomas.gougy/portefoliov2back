import { IsBoolean, IsDate, IsNotEmpty, IsString } from "class-validator"


export class CreateExperienceDto{
    @IsString()
    @IsNotEmpty()
    title: string 
    @IsString()
    @IsNotEmpty()
    enterpriseName : string 
    @IsString()
    @IsNotEmpty()
    enterpriseImg: string
    @IsString()
    @IsNotEmpty() 
    presentation: string 
    @IsNotEmpty()
    @IsString()
    startDate: Date
    @IsNotEmpty()
    @IsString()
    endDate: Date
    @IsBoolean()
    inJob : boolean
    @IsString()
    @IsNotEmpty()
    userId: string
}

export class UpdateExperienceDto{
    @IsString()
    @IsNotEmpty()
    experienceId: string
    @IsString()
    @IsNotEmpty()
    title: string 
    @IsString()
    @IsNotEmpty()
    enterpriseName : string 
    @IsString()
    @IsNotEmpty()
    enterpriseImg: string
    @IsString()
    @IsNotEmpty() 
    presentation: string 
    @IsNotEmpty()
    startDate: Date
    @IsNotEmpty()
    endDate: Date
    @IsBoolean()
    inJob : boolean
    @IsString()
    @IsNotEmpty()
    userId: string
}
