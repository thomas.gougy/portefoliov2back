import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ExperienceService } from './experience.service';
import { CreateExperienceDto, UpdateExperienceDto } from './dto/experience.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Experience')
@Controller('experience')
export class ExperienceController {

    constructor(private readonly experienceService: ExperienceService){}

    @Get("user/:id")
    getUserExperience(@Param('id')userId: string){
       return this.experienceService.getUserExperience(userId)
    }

    @Get(":id")
    getOneExperience(@Param('id')experienceId: string){
        return this.experienceService.getOneExperience(experienceId)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("")
    createOneExperience(@Body()experienceDto: CreateExperienceDto){
        return this.experienceService.createOneExperience(experienceDto)
    }


    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("multiple")
    createMultipleExperience(@Body()experienceDto: CreateExperienceDto[]){
        return this.experienceService.createMultipleExperience(experienceDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Put()
    updateOneExperience(@Body()experienceDto: UpdateExperienceDto){
        return this.experienceService.updateOneExperience(experienceDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Delete("/:id")
    deleteOneExperience(@Param("id")experienceId: string){
        return this.experienceService.deleteOneExperience(experienceId)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt"))
    @Post("delete/multiple")
    deleteMultipleExperience(@Body()experienceIds: string[]){
        return this.experienceService.deleteMultipleExperience(experienceIds)
    }

}
