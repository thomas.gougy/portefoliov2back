import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateExperienceDto, UpdateExperienceDto } from './dto/experience.dto';
import { v4 } from 'uuid';

@Injectable()
export class ExperienceService {


    constructor( private readonly prismaService: PrismaService,){
        
    }


    async getUserExperience(userId: string){

       const experiences = await this.prismaService.experience.findMany({where:{userId: userId}})

       return {experiences: experiences}

    }

    async getOneExperience(experienceId: string) {
        const experience = await this.prismaService.experience.findUnique({where:{experienceId: experienceId}})
        if(!experience) throw new NotFoundException("experience can't be found")
        return experience
    }

    async createOneExperience(experienceDto: CreateExperienceDto){

        const experience = await this.prismaService.experience.create({data: experienceDto})

        return experience
    }

    async createMultipleExperience(experienceDto: CreateExperienceDto[]){

        const defineExperience: any [] = []
        const ids:string[] = []
        let id 


        for(let experience of experienceDto){
            id = v4()
            defineExperience.push({experienceId :id, ...experience})
            ids.push(id)
        }
        const experiences = await this.prismaService.experience.createMany({data: defineExperience})
        const values = await this.prismaService.experience.findMany({where: {experienceId: {in: ids}}})

        return values
    }

    async updateOneExperience(experienceDto : UpdateExperienceDto){

        const experience = await this.verifyExperienceExist(experienceDto.experienceId)

        experience.title = experienceDto.title
        experience.enterpriseName = experienceDto.enterpriseName
        experience.enterpriseImg = experienceDto.enterpriseImg
        experience.presentation = experienceDto.presentation
        experience.startDate = experienceDto.startDate
        experience.endDate = experienceDto.endDate
        experience.inJob = experienceDto.inJob

        const updatedExperience = await this.prismaService.experience.update({where: {experienceId: experienceDto.experienceId}, data: experience})

        return updatedExperience
    }

    async deleteOneExperience(experienceId: string){
        const experiences = await this.verifyExperienceExist(experienceId)

        await this.prismaService.experience.delete({where: { experienceId: experiences.experienceId}})
        return {msg:"Experience has been deleted"}
    }

    async deleteMultipleExperience(experienceIds: string[]){
     
        for(let id of experienceIds){
            this.deleteOneExperience(id)
        }

        return{msg: "Experiences has been delete"}
    }




    async verifyExperienceExist(experienceId: string){

        const experience = await this.prismaService.experience.findUnique({where: {experienceId: experienceId}})

        if(!experience) throw new NotFoundException("Experience can't be found")

        return experience
    }
}
